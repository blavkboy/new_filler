/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tester.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:12:52 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/05 07:03:16 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../libft/libft.h"
#include <fcntl.h>
#include <stdio.h>

typedef struct		s_moves
{
	size_t			*play;
	struct s_moves	*next;
}					t_moves;

typedef struct		s_game
{
	unsigned		on 	:1;
	unsigned		p	:2;
	size_t			dim[2];
	size_t			token_dim[2];
	char			piece;
	char			opp;
	char			**map;
	char			**token;
	t_moves			*moves;
	size_t			offset[4];
}					t_game;

void		determine_player(t_game *game);
void		put_coord(t_game *game, size_t y, size_t x);

static void	bottom_offset(t_game *game)
{
	size_t	x;
	int		y;
	int		end;
	size_t	off;

	y = (int)game->token_dim[0] - 1;
	off = 0;
	end = 0;
	while (y >= 0 && !end)
	{
		x = 0;
		while (game->token[y][x] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			x++;
		}
		if (end)
			break ;
		else
			off++;
		y--;
	}
	game->offset[3] = off;
}

static void	right_offset(t_game *game)
{
	int		x;
	size_t	y;
	int		end;
	size_t off;

	off = 0;
	x = game->token_dim[1] - 1;
	end = 0;
	while (x >= 0 && !end)
	{
		y = 0;
		while (game->token[y] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			y++;
		}
		if (end)
			break ;
		off++;
		x--;
	}
	game->offset[2] = off;
}

static void	top_offset(t_game *game)
{
	size_t	x;
	size_t	y;
	int		end;

	y = 0;
	end = 0;
	while (y < game->token_dim[0] && !end)
	{
		x = 0;
		while (x < game->token_dim[1] && !end)
		{
			if (game->token[y][x] == '*')
				end  = 1;
			x++;
		}
		if (end)
			break ;
		y++;
	}
	game->offset[1] = y;
}

static void	left_offset(t_game *game)
{
	size_t	x;
	size_t	y;
	int		end;

	end = 0;
	x = 0;
	while (x < game->token_dim[1] && !end)
	{
		y = 0;
		while (y < game->token_dim[0] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			y++;
		}
		if (end)
			break ;
		x++;
	}
	game->offset[0] = x;
}

void		calculate_offset(t_game *game)
{
	left_offset(game);
	right_offset(game);
	top_offset(game);
	bottom_offset(game);
}

//restarting filler to accomodate for any version of filler

t_game		*init_game(void)
{
	t_game	*game;

	if ((game = ft_memalloc(sizeof(t_game))) == NULL)
		return (NULL);
	game->on = 1;
	game->p = 0;
	game->map = NULL;
	game->moves = NULL;
	game->token = NULL;
	ft_bzero((void*)&game->token_dim, sizeof(size_t) * 2);
	ft_bzero((void*)&game->offset, sizeof(size_t) * 4);
	ft_bzero((void*)&game->dim, sizeof(size_t) * 2);
	determine_player(game);
	return (game);
}

void		determine_player(t_game *game)
{
	char	*line;
	char	*ptr;

	line = NULL;
	if (game->on)
	{
		if (get_next_line(0, &line) < 1)
			game->on = 0;
		if (game->on)
		{
			ptr = line;
			while (!ft_isdigit(*ptr))
				ptr++;
			game->p = ft_atoi(ptr);
		}
		ft_strdel(&line);
		if (game->p == 1)
			game->piece = 'o';
		else if (game->p == 2)
			game->piece = 'x';
	}
}

void		map_dimensions(t_game *game)
{
	char	*line;
	int		x;
	int		y;

	line = NULL;
	x = 0;
	y = 0;
	if (game->on)
	{
		get_next_line(0, &line);
		while (line[x])
		{
			if (ft_isdigit(line[x]))
			{
				game->dim[y] = ft_atoi(&line[x]);
				y++;
				while (ft_isdigit(line[x]) && line[x])
					x++;
			}
			if (y > 1)
				break ;
			x++;
		}
		ft_strdel(&line);
	}
}

void		get_map(t_game *game)
{
	size_t	y;
	char	*line;

	y = 0;
	line = NULL;
	if (game->on)
	{
		get_next_line(0, &line);
		ft_strdel(&line);
		if ((game->map = ft_memalloc(sizeof(char**) * (game->dim[0] + 1)))
			== NULL)
		{
			game->on = 0;
			return ;
		}
		while (y < game->dim[0])
		{
			get_next_line(0, &line);
			game->map[y] = ft_strsub(line, 4, game->dim[1]);
			ft_strdel(&line);
			y++;
		}
		game->map[y] = NULL;
	}
}

void		get_token_dim(t_game *game)
{
	size_t	y;
	size_t	x;
	char	*line;

	y = 0;
	x = 0;
	line = NULL;
	if (game->on)
	{
		get_next_line(0, &line);
		while (line[x])
		{
			if (ft_isdigit(line[x]))
			{
				game->token_dim[y] = ft_atoi(&line[x]);
				while (ft_isdigit(line[x]) && line[x])
					x++;
				y++;
			}
			if (y > 1)
				break ;
			x++;
		}
		ft_strdel(&line);
	}
}

void		read_token(t_game *game)
{
	char	*line;
	size_t	i;

	i = 0;
	line = 0;
	if (game->on == 0)
		return ;
	game->token = ft_memalloc(sizeof(char**) * (game->token_dim[0] + 1));
	while (i < game->token_dim[0])
	{
		get_next_line(0, &line);
		game->token[i] = ft_strsub(line, 0, ft_strlen(line));
		ft_strdel(&line);
		i++;
	}
	game->token[i] = NULL;
	calculate_offset(game);
}

//capture all the available moves into a linked list.
void		get_move(t_game *game, size_t y, size_t x)
{
	t_moves	*move;

	if (game->moves)
	{
		move = game->moves;
		while (move->next)
			move = move->next;
		move->next = ft_memalloc(sizeof(t_moves));
		move->next->next = NULL;
		move->next->play = ft_memalloc(sizeof(size_t) * 2);
		move->next->play[0] = y;
		move->next->play[1] = x;
	}
	else
	{
		game->moves = ft_memalloc(sizeof(t_moves));
		game->moves->next = NULL;
		game->moves->play = ft_memalloc(sizeof(size_t));
		game->moves->play[0] = y;
		game->moves->play[1] = x;
	}
}

//measure distance between coordinate and the closest enemy node.
long long		distance(t_moves *move, size_t y, size_t x)
{
	return (ft_power(move->play[1] - x, 2) + ft_power(move->play[0] - y, 2));
}

//compare distance between moves.
t_moves			*dst_compare(long long *len, t_game *game, t_moves *moves)
{
	size_t		y;
	size_t		x;
	long long	start;
	char		opp;

	y = 0;
	start = *len;
	if (ft_tolower(game->piece) == 'o')
		opp = 'x';
	else
		opp = 'o';
	while (y < game->dim[0])
	{
		x = 0;
		while (x < game->dim[1])
		{
			if (ft_tolower(game->map[y][x]) == opp)
			{
				put_coord(game, moves->play[0], moves->play[1]);
				ft_putsgreen("Current coordinate.\n");
				ft_putscyan(ft_itoa(distance(moves, y, x)));
				ft_putscyan(" <- current distance\n");
				if (distance(moves, y, x) < start)
				{
					start = distance(moves, y, x);
				}
			}
			x++;
		}
		y++;
	}
	if (*len > start)
	{
		//put_coord(game, moves->play[0], moves->play[1]);
		*len = start;
		ft_putsred("start -> ");
		ft_putendl(ft_itoa(start));
		return (moves);
	}
	return (NULL);
}

//select the best move to play based on pythegoras theorum.
void			pick_move(t_game *game)
{
	t_moves		*moves;
	t_moves		*move;
	t_moves		*test;
	long long	len;
	size_t		itr;

	move = NULL;
	len = game->dim[0] * game->dim[1];
	itr = 0;
	ft_putchar('\n');
	if (game->moves)
	{
		moves = game->moves;
		while (moves)
		{
			if ((test = dst_compare(&len, game, moves)) != NULL)
			{
				move = test;
			}
			moves = moves->next;
			itr++;
		}
		ft_putscyan("\nFinal choice: ");
		if (move)
			put_coord(game, move->play[0], move->play[1]);
	}
	else
		ft_putendl("");
}

//Hear ye, hear ye... these next functions will collect and print out coordinates.
int		check_validity(t_game *game, size_t y, size_t x)
{
	size_t	h;
	size_t	w;
	size_t	count;
	char	opp;

	count = 0;
	h = 0;
	if (game->piece == 'o')
		opp = 'x';
	else
		opp = 'o';
	while (h < game->token_dim[0] - (game->offset[1] + game->offset[3]))
	{
		w = 0;
		/*if (y == 0 && x == 0)
		{
			ft_putsyellow("Checking Height: ");
			ft_putsred(ft_itoa(h));
			ft_putchar('\n');
		}*/
		while (w < game->token_dim[1] - (game->offset[0] + game->offset[2]))
		{
			/*if (y == 0 && x == 0)
			{
				ft_putscyan("Checking width: ");
				ft_putsred(ft_itoa(w));
				ft_putchar('\n');
				ft_putspurple("Symbol found at coordinate in map: ");
				ft_putchar(game->map[y + h][x + w]);
				ft_putspurple("\nSymbol from token: ");
				ft_putchar(game->token[h + game->offset[1]][w + game->offset[0]]);
			}*/
			if (ft_tolower(game->map[y + h][x + w]) == game->piece &&
					game->token[h + game->offset[1]][w + game->offset[0]] == '*')
				count++;
			if (ft_tolower(game->map[y + h][x + w]) == opp &&
					game->token[h + game->offset[1]][w + game->offset[0]] == '*')
				count += 2;
			w++;
		}
		h++;
	}
	/*ft_putsyellow("y value: ");
	ft_putnbr(y);
	ft_putchar('\n');
	ft_putsred("x value: ");
	ft_putnbr(x);
	ft_putchar('\n');
	*/if (count == 1)
		return (1);
	return (0);
}

void		put_coord(t_game *game, size_t y, size_t x)
{
	ft_putnbr(y - game->offset[1]);
	ft_putchar(' ');
	ft_putnbr(x - game->offset[0]);
	ft_putendl("");
}

void		get_coord(t_game *game)
{
	size_t	y;
	size_t	x;

	y = 0;
	while (y + (game->token_dim[0] - (game->offset[1] + game->offset[3])) <= game->dim[0])
	{
		x = 0;
		while (x + (game->token_dim[1] -  (game->offset[0] + game->offset[2])) <= game->dim[1])
		{
			//if (check_validity(game, y, x))
			//	put_coord(game, y, x);
			if (check_validity(game, y, x))
				get_move(game, y, x);
			x++;
		}
		y++;
	}
}

//Memory management functions.
void		destroy_moves(t_game *game)
{
	t_moves *moves;
	t_moves *next;

	moves = game->moves;
	next = moves->next;
	game->moves = NULL;
	while(moves)
	{
		free(moves->play);
		ft_memdel((void**)&moves);
		moves = next;
		if (next)
			next = next->next;
	}
}

void		matrix_free(char **map)
{
	int		y;

	y = 0;
	while (map[y] != NULL)
	{
		ft_strdel(&map[y]);
		y++;
	}
}

void		clean_map(t_game *game)
{
	char	**map;

	map = game->map;
	game->map = NULL;
	matrix_free(map);
	ft_memdel((void**)map);
}

void		clean_token(t_game *game)
{
	char	**token;

	token = game->token;
	game->token = NULL;
	matrix_free(token);
	ft_memdel((void**)token);
}

void		reset_token(t_game *game)
{
	game->token_dim[0] = 0;
	game->token_dim[1] = 0;
}

void		reset_offset(t_game *game)
{
	game->offset[0] = 0;
	game->offset[1] = 0;
	game->offset[2] = 0;
	game->offset[3] = 0;
}

void		free_round(t_game *game)
{
	ft_putsred("1. ");
	ft_putscyan("Clean linked list: \n");
	destroy_moves(game);
	if (!game->moves)
		ft_putsgreen("Passed the test!\n");
	else
		ft_putsred("Failed the test!\n");
	ft_putsred("\n2. ");
	ft_putscyan("Clean map: \n");
	clean_map(game);
	if (!game->map)
		ft_putsgreen("Passed the test.\n");
	else
		ft_putsred("Failed the test!\n");
	ft_putsred("\n.3 ");
	ft_putscyan("Clean token: \n");
	clean_token(game);
	if (!game->token)
		ft_putsgreen("Passed Test.\n");
	else
		ft_putsred("Failed Test!\n");
	ft_putsred("\n3.1 ");
	ft_putscyan("Reset token dim: \n");
	reset_token(game);
	if (!game->token_dim[0] && !game->token_dim[1])
		ft_putsgreen("Passed Test.\n");
	else
		ft_putsred("Failed Test!\n");
	ft_putsred("\n4. ");
	ft_putscyan("Reset offset: \n");
	reset_offset(game);
	if (!game->offset[0] && !game->offset[1] && !game->offset[2] && !game->offset[3])
		ft_putsgreen("Passed Test.\n");
	else
		ft_putsred("Failed Test!\n");
}

void		destroy_game(t_game **game)
{
	ft_memdel((void**)game);
}

int			main(void)
{
	t_game *game;

	//testing initialization of the game piece and
	//getting a piece for the player.
	game = init_game();
	ft_putscyan("Testing determine_player() function.\n");
	ft_putspurple("You are player number: ");
	ft_putsgreen(ft_itoa(game->p));
	ft_putspurple("\nYour game piece is: ");
	ft_putchar(game->piece);
	ft_putchar('\n');
	//Testing map dimension functions from here.
	map_dimensions(game);
	ft_putscyan("\nTesting reading of map dimesnions: \n");
	ft_putsgreen("game->dim[0]: ");
	ft_putnbr(game->dim[0]);
	ft_putsgreen("\ngame->dim[1]: ");
	ft_putnbr(game->dim[1]);
	ft_putchar('\n');
	ft_putscyan("Testing get_map() function.\n");
	get_map(game);
	size_t	itr = 0;
	while (itr < game->dim[0])
	{
		ft_putnbr(itr);
		ft_putstr(":\t");
		ft_putendl(game->map[itr]);
		itr++;
	}
	itr = 0;
	ft_putchar('\n');
	//Tests for token dimensions and reading the actual token itself.
	get_token_dim(game);
	ft_putscyan("Testing the get_token_dim() function.\n");
	ft_putspurple("game->token_dim[0]: ");
	ft_putsgreen(ft_itoa(game->token_dim[0]));
	ft_putspurple("\ngame->token_dim[1]: ");
	ft_putsgreen(ft_itoa(game->token_dim[1]));
	ft_putchar('\n');
	read_token(game);
	ft_putscyan("Testing read_token() function.\n");
	while (itr < game->token_dim[0])
	{
		ft_putnbr(itr);
		ft_putstr(":\t");
		ft_putendl(game->token[itr]);
		itr++;
	}
	ft_putscyan("\n\nTesting recognition of possible coordinates.\n");
	ft_putsred("1. ");
	ft_putscyan("Offsets: \n");
	ft_putsgreen("game->offset[0] = ");
	ft_putnbr(game->offset[0]);
	ft_putsgreen("\ngame->offset[1] = ");
	ft_putnbr(game->offset[1]);
	ft_putsgreen("\ngame->offset[2] = ");
	ft_putnbr(game->offset[2]);
	ft_putsgreen("\ngame->offset[3] = ");
	ft_putnbr(game->offset[3]);
	ft_putchar('\n');
	ft_putsred("\n2. ");
	ft_putscyan("Testing found coordinates: \n");
	get_coord(game);
	t_moves	*moves;
	moves = game->moves;
	while (moves)
	{
		put_coord(game, moves->play[0], moves->play[1]);
		moves = moves->next;
	}
	ft_putsred("\n3. ");
	ft_putscyan("Tesing coordinate picking: \n");
	pick_move(game);
	ft_putscyan("\n\nTesting memory management functions: \n\n");
	free_round(game);
	ft_putscyan("\n\nTesting game free:");
	moves = NULL;
	destroy_game(&game);
	game = NULL;
	if (game == NULL)
		ft_putsgreen("\n\nPassed Test: Removed game from memory.\n");
	else
		ft_putsred("\n\nFailed!!!\n");
	return (0);
}
