/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main2.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/06 16:25:45 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/06 16:34:16 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		initialize(t_game *game)
{
	char	*line;

	line = NULL;
	get_next_line(0, &line);
	map_dimensions(game, line);
	get_map(game);
	get_token_dim(game);
	read_token(game);
}

int			main(void)
{
	t_game	*game;
	char	*line;

	line = NULL;
	game = init_game();
	while (1)
	{
		if (!get_next_line(0, &line))
			break ;
		map_dimensions(game, line);
		get_map(game);
		get_token_dim(game);
		read_token(game);
		get_coord(game);
		pick_move(game);
		free_round(game);
	}
	ft_putendl("Got here!");
	destroy_game(&game);
	return (0);
}

