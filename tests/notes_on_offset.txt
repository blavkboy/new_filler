With my bot reading the pieces at it is, we come across two problems.
1. 	The first being that we could be checking too much of the token.
2.	The second being that we don't get to know all the possible places
	to put our token. Most of the bots we are beating purely by an algorithm
	that places pieces well without even trying to play against them.

To rememdy this we calculate the offset clockwise starting from the left of
the token. My problem now if figuring how to put that in my program
to help make it more effecient in the time it spends checking if a particular
spot on the map is valid to place the piece in. I could just check the amount
of the token that actually has an obstruction in it. The bot doesn't need to
do anything hectic, but it should follow the simple strategy of trying to go
around the opponent and making it hard to close it off or confine it to one place.
That being said it should still fill up the rest of the map which should be default
if you think about it.

Now that the offset seems to be working and I can find spaces to place my pieces. A
new objective has come up. We should store all the possible moves for that turn in a
linked list and eveluate the best move to make based on the opponents position relative
to our own.
1.	Create a linked list to store all the moves for that turn.
2.	Find a way to pick the move that is moving towards the enemy pieces.
3.	Surround the enemy with the placements that follow.
4.	Fill up the rest of the map.

*	As a possible solution, we will turn the possible moves into a tree with as man nodes as possible
	and use minimax algorithm to get the best position possible to pick out of the moves available to us.
*	Another is to simply head towards the quadrant that the opponent is in and fill up the space around their
	piece so that we don't have to contend with in the other open spaces. The algorithm should find the closest
	open opponent piece and moves towards it so it can cut it off before filling up the remainder of the map.

The second solution seems to be the best choice I could make. It won't be easy but the algorithim should walk
through the entire map and take note of all the positions occupied by the opponent and the bot we are controlling.
It should then relay that information back to the bot by choosing the coordinate that is closest to an enemy piece.

I suspect that the values I expect to see being output are faiing due to my amature knowledge in regards to pointers
and memory. Need to check out my usage of memory here and correct that issue before I even deal with the loop issue.

Solved placing and now it's time to put this whole thing going round in a loop.
