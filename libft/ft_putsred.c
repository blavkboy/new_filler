/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putsred.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/13 12:18:30 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/06/14 14:33:07 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_putsred(char const *str)
{
	ft_putstr("\033[0;31m");
	ft_putstr(str);
	ft_putstr("\033[0m");
}
