# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/07/06 14:59:58 by gmohlamo          #+#    #+#              #
#    Updated: 2018/07/14 07:29:24 by gmohlamo         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = filler
SRCS = srcs/calculate.c srcs/game.c srcs/decide.c srcs/digits.c srcs/memory.c \
	   srcs/moves.c srcs/readers.c srcs/offset.c srcs/round.c srcs/graphics.c srcs/main.c
OBJ = calculate.o moves.o readers.o offset.o memory.o round.o game.o \
	  decide.o digits.o graphics.o main.o
FLAGS = -lmlx -framework OpenGL -framework AppKit -L./libft -lft -Wall -Wextra -Werror -g

$(NAME): $(OBJ)
	gcc -o filler $(OBJ) $(FLAGS)

all: $(NAME)

$(OBJ):
	gcc -c $(SRCS) -Wall -Wextra -Werror

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f filler

re: fclean all

bot: re
	make clean
