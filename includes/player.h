/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/09 13:02:59 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/06 16:18:21 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PLAYER_H
# define PLAYER_H

# include "../libft/libft.h"
# include <mlx.h>
# include <fcntl.h>

typedef struct	s_mlx
{
	void		*mlx;
	void		*window;
	void		*image;
	char		*data;
	int			y_window;
	int			x_window;
	int			bpp;
	int			size_line;
	int			endian;
}				t_mlx;

typedef struct		s_moves
{
	size_t			*play;
	struct s_moves	*next;
}					t_moves;

typedef struct		s_game
{
	unsigned		on 	:1;
	unsigned		p	:2;
	size_t			dim[2];
	size_t			token_dim[2];
	char			piece;
	char			opp;
	char			**map;
	char			**token;
	t_moves			*moves;
	size_t			offset[4];
	t_mlx			*mlx;
}					t_game;

void				destroy_game(t_game **game);
void				clean_map(t_game *game);
void				free_round(t_game *game);
long long			distance(t_moves *move, size_t y, size_t x);
t_moves				*dst_compare(long long *len, t_game *game, t_moves *moves);
void				pick_move(t_game *game);
int					check_validity(t_game *game, size_t y, size_t x);
void				put_coord(t_game *game, size_t y, size_t x);
void				get_coord(t_game *game);
void				reset_token(t_game *game);
void				reset_offset(t_game *game);
void				free_round(t_game *game);
t_game				*init_game(void);
void				determine_player(t_game *game);
void				destroy_moves(t_game *game);
void				matrix_free(char **map);
void				clean_token(t_game *game);
void				get_move(t_game *game, size_t y, size_t x);
void				calculate_offset(t_game *game);
void				map_dimensions(t_game *game, char *line);
void				get_map(t_game *game);
void				get_token_dim(t_game *game);
void				read_token(t_game *game);
t_mlx				*init_graphics(int y, int x, char *name);
void				mlx_put_image(t_mlx *mlx, int x, int y, int *color);
void				make_rect(t_mlx *mlx, int h, int w, int size_w, int size_h, int color[3]);
void				draw_map(t_mlx *mlx, size_t h, size_t w);
void				draw_players(t_game *game, t_mlx *mlx);

# endif
