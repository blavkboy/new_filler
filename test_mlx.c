/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_mlx.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/12 08:49:18 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/13 13:21:36 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <mlx.h>
#include "./libft/libft.h"
#include "./includes/player.h"

typedef struct	s_mlx
{
	void		*mlx;
	void		*window;
	void		*image;
	char		*data;
	int			y_window;
	int			x_window;
	int			bpp;
	int			size_line;
	int			endian;
}				t_mlx;

t_mlx		*init_graphics(int y, int x, char *name)
{
	t_mlx	*mlx;

	mlx = ft_memalloc(sizeof(t_mlx));
	mlx->y_window = y;
	mlx->x_window = x;
	mlx->mlx = mlx_init();
	mlx->window = mlx_new_window(mlx->mlx, mlx->x_window, mlx->y_window, name);
	mlx->image = mlx_new_image(mlx->mlx, mlx->x_window, mlx->y_window);
	mlx->data = mlx_get_data_addr(mlx->image, &(mlx->bpp), &(mlx->size_line), &(mlx->endian));
	return (mlx);
}

void		mlx_put_image(t_mlx *mlx, int x, int y, int *color)
{
	mlx->data[(y * mlx->size_line) + ((x * (mlx->bpp / 8)))] = mlx_get_color_value(mlx->mlx, color[0]);
	mlx->data[((y * mlx->size_line) + ((x * (mlx->bpp / 8)))) + 1] = mlx_get_color_value(mlx->mlx, color[1]);
	mlx->data[((y * mlx->size_line) + ((x * (mlx->bpp / 8)))) + 2] = mlx_get_color_value(mlx->mlx, color[2]);
}

void		make_rect(t_mlx *mlx, int h, int w, int size_w, int size_h, int color[3])
{
	int		y;
	int		x;

	y = 0;
	while (y < size_h)
	{
		x = 0;
		while (x < size_w)
		{
			mlx_put_image(mlx, w + x, h + y, color);
			x++;
		}
		y++;
	}
}

/*
**	draw_map() is the most vital part of this function. 
**	It prints the map to the screen. Player 1 should be red and
**	player 2 should be blue. The game will keep posting to the screen
**	so long as the bot calling it is playing.
*/

void		draw_map(t_mlx *mlx, size_t h, size_t w)
{
	int		y;
	int		x;
	int		Grey_c[3] = {50, 50, 50};
	int		White_c[3] = {255, 255, 255};

	y = 0;
	make_rect(mlx, 0, 0, w * 10, h * 10, White_c);
	while (y < h)
	{
		x = 0;
		while (x < w)
		{
			make_rect(mlx, (y * 10) + 2, (x * 10), 10 - 1, 10 - 1, Grey_c);
			x++;
		}
		y++;
	}
}

//remember BGR not RBG

void		draw_players(t_game *game, t_mlx *mlx)
{
	int		x;
	int		y;
	int		Red_c[3] = {0, 0, 128};
	int		Blue_c[3] = {128, 0, 0};

	y = 0;
	while (y < game->dim[0])
	{
		x = 0;
		while (x < game->dim[1])
		{
			if (ft_tolower(game->map[y][x]) == 'x')
				make_rect(mlx, (y * 10) + 2, (x * 10), 10 - 1, 10 - 1, Red_c);
			else if (ft_tolower(game->map[y][x]) == 'o')
				make_rect(mlx, (y * 10) + 1, (x * 10), 10 - 1, 10 - 1, Blue_c);
			x++;
		}
		y++;
	}
}

int					main(void)
{
	t_mlx			*mlx;
	t_game			*game;

	game = ft_memalloc(sizeof(t_game));
	game->dim[0] = 100;
	game->dim[1] = 99;
	mlx = init_graphics(game->dim[0] * 10, game->dim[1] * 10, "test");
	draw_map(mlx, game->dim[0], game->dim[1]);
	mlx_put_image_to_window(mlx->mlx, mlx->window, mlx->image, 0, 0);
	mlx_loop(mlx->mlx);
	return (0);
}
