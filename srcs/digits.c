/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   digits.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:11:22 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/06 15:23:02 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		reset_token(t_game *game)
{
	game->token_dim[0] = 0;
	game->token_dim[1] = 0;
}

void		reset_offset(t_game *game)
{
	game->offset[0] = 0;
	game->offset[1] = 0;
	game->offset[2] = 0;
	game->offset[3] = 0;
}

void		destroy_game(t_game **game)
{
	ft_memdel((void**)game);
}

