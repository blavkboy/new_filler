/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   moves.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:15:29 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/07 13:40:23 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		get_move(t_game *game, size_t y, size_t x)
{
	t_moves	*move;

	if (game->moves)
	{
		move = game->moves;
		while (move->next)
			move = move->next;
		move->next = ft_memalloc(sizeof(t_moves));
		move->next->next = NULL;
		move->next->play = ft_memalloc(sizeof(size_t) * 2);
		move->next->play[0] = y;
		move->next->play[1] = x;
	}
	else
	{
		game->moves = ft_memalloc(sizeof(t_moves));
		game->moves->next = NULL;
		game->moves->play = ft_memalloc(sizeof(size_t));
		game->moves->play[0] = y;
		game->moves->play[1] = x;
	}
}
