/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   offset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:05:32 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/04 17:06:34 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

static void	bottom_offset(t_game *game)
{
	size_t	x;
	int		y;
	int		end;
	size_t	off;

	y = (int)game->token_dim[0] - 1;
	off = 0;
	end = 0;
	while (y >= 0 && !end)
	{
		x = 0;
		while (game->token[y][x] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			x++;
		}
		if (end)
			break ;
		else
			off++;
		y--;
	}
	game->offset[3] = off;
}

static void	right_offset(t_game *game)
{
	int		x;
	size_t	y;
	int		end;
	size_t off;

	off = 0;
	x = game->token_dim[1] - 1;
	end = 0;
	while (x >= 0 && !end)
	{
		y = 0;
		while (game->token[y] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			y++;
		}
		if (end)
			break ;
		off++;
		x--;
	}
	game->offset[2] = off;
}

static void	top_offset(t_game *game)
{
	size_t	x;
	size_t	y;
	int		end;

	y = 0;
	end = 0;
	while (y < game->token_dim[0] && !end)
	{
		x = 0;
		while (x < game->token_dim[1] && !end)
		{
			if (game->token[y][x] == '*')
				end  = 1;
			x++;
		}
		if (end)
			break ;
		y++;
	}
	game->offset[1] = y;
}

static void	left_offset(t_game *game)
{
	size_t	x;
	size_t	y;
	int		end;

	end = 0;
	x = 0;
	while (x < game->token_dim[1] && !end)
	{
		y = 0;
		while (y < game->token_dim[0] && !end)
		{
			if (game->token[y][x] == '*')
				end = 1;
			y++;
		}
		if (end)
			break ;
		x++;
	}
	game->offset[0] = x;
}

void		calculate_offset(t_game *game)
{
	left_offset(game);
	right_offset(game);
	top_offset(game);
	bottom_offset(game);
}
