/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   round.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 14:15:59 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/05 14:18:15 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		free_round(t_game *game)
{
	destroy_moves(game);
	clean_map(game);
	clean_token(game);
	reset_token(game);
	reset_offset(game);
}

