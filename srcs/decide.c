/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   decide.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:31:42 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/09 14:43:13 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void			pick_move(t_game *game)
{
	t_moves		*moves;
	t_moves		*move;
	t_moves		*test;
	long long	len;
	size_t		itr;

	move = NULL;
	len = 2000000000;
	itr = 0;
	if (game->moves)
	{
		moves = game->moves;
		while (moves)
		{
			if ((test = dst_compare(&len, game, moves)) != NULL)
				move = test;
			moves = moves->next;
			itr++;
		}
		if (move)
			put_coord(game, move->play[0], move->play[1]);
	}
	else
	{
		game->on = 0;
		ft_putendl("0 0");
	}
}

int		check_validity(t_game *game, size_t y, size_t x)
{
	size_t	h;
	size_t	w;
	size_t	count;

	count = 0;
	h = 0;
	while (h < game->token_dim[0] - (game->offset[1] + game->offset[3]))
	{
		w = 0;
		while (w < game->token_dim[1] - (game->offset[0] + game->offset[2]))
		{
			if (ft_tolower(game->map[y + h][x + w]) == game->piece &&
					game->token[h + game->offset[1]][w + game->offset[0]] == '*')
				count++;
			if (ft_tolower(game->map[y + h][x + w]) == game->opp &&
					game->token[h + game->offset[1]][w + game->offset[0]] == '*')
				count += 2;
			w++;
		}
		h++;
	}
	if (count == 1)
		return (1);
	return (0);
}

void		put_coord(t_game *game, size_t y, size_t x)
{
	ft_putnbr(y - game->offset[1]);
	ft_putchar(' ');
	ft_putnbr(x - game->offset[0]);
	ft_putendl("");
}

void		get_coord(t_game *game)
{
	size_t	y;
	size_t	x;

	y = 0;
	while (y + (game->token_dim[0] - (game->offset[1] + game->offset[3])) <= game->dim[0])
	{
		x = 0;
		while (x + (game->token_dim[1] -  (game->offset[0] + game->offset[2])) <= game->dim[1])
		{
			if (check_validity(game, y, x))
				get_move(game, y, x);
			x++;
		}
		y++;
	}
}
