/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:10:25 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/09 14:43:54 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		destroy_moves(t_game *game)
{
	t_moves *moves;
	t_moves *next;

	if (!game->moves)
		return ;
	moves = game->moves;
	next = moves->next;
	game->moves = NULL;
	while(moves)
	{
		free(moves->play);
		ft_memdel((void**)&moves);
		moves = next;
		if (next)
			next = next->next;
	}
}

void		matrix_free(char **map)
{
	int		y;

	y = 0;
	while (map[y] != NULL)
	{
		ft_strdel(&map[y]);
		y++;
	}
	ft_strdel(&map[y]);
}

void		clean_map(t_game *game)
{
	matrix_free(game->map);
	ft_memdel((void**)game->map);
}

void		clean_token(t_game *game)
{
	matrix_free(game->token);
	ft_memdel((void**)game->token);
}
