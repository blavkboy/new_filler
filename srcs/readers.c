/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:04:57 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/16 16:49:49 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

void		map_dimensions(t_game *game, char *line)
{
	int		x;
	int		y;

	x = 0;
	y = 0;
	if (game->on)
	{
		while (line[x])
		{
			if (ft_isdigit(line[x]))
			{
				game->dim[y] = ft_atoi(&line[x]);
				y++;
				while (ft_isdigit(line[x]) && line[x])
					x++;
			}
			if (y > 1)
				break ;
			x++;
		}
		ft_strdel(&line);
	}
}

void		get_map(t_game *game)
{
	size_t	y;
	char	*line;

	y = 0;
	line = NULL;
	if (game->on)
	{
		get_next_line(0, &line);
		ft_strdel(&line);
		if ((game->map = ft_memalloc(sizeof(char**) * (game->dim[0] + 1)))
			== NULL)
		{
			game->on = 0;
			return ;
		}
		while (y < game->dim[0])
		{
			get_next_line(0, &line);
			game->map[y] = ft_strsub(line, 4, game->dim[1]);
			ft_strdel(&line);
			y++;
		}
		game->map[y] = NULL;
	}
}

void		get_token_dim(t_game *game)
{
	size_t	y;
	size_t	x;
	char	*line;

	y = 0;
	x = 0;
	line = NULL;
	if (game->on)
	{
		get_next_line(0, &line);
		while (line[x])
		{
			if (ft_isdigit(line[x]))
			{
				game->token_dim[y] = ft_atoi(&line[x]);
				while (ft_isdigit(line[x]) && line[x])
					x++;
				y++;
			}
			if (y > 1)
				break ;
			x++;
		}
		ft_strdel(&line);
	}
}

void		read_token(t_game *game)
{
	char	*line;
	size_t	i;

	i = 0;
	line = 0;
	if (game->on == 0)
		return ;
	game->token = ft_memalloc(sizeof(char**) * (game->token_dim[0] + 1));
	while (i < game->token_dim[0])
	{
		get_next_line(0, &line);
		game->token[i] = ft_strsub(line, 0, ft_strlen(line));
		ft_strdel(&line);
		i++;
	}
	game->token[i] = NULL;
	calculate_offset(game);
}
