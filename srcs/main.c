/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/05 14:35:10 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/16 16:47:42 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

int			main(void)
{
	t_game	*game;
	char	*line;

	line = NULL;
	game = init_game();
	get_next_line(0, &line);
	while (game->on)
	{
		if (!ft_strstr(line, "Plateau"))
			break ;
		map_dimensions(game, line);
		get_map(game);
		get_token_dim(game);
		read_token(game);
		get_coord(game);
		pick_move(game);
		free_round(game);
		get_next_line(0, &line);
	}
	destroy_game(&game);
	return (0);
}
