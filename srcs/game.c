/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:09:30 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/06 15:58:32 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

t_game		*init_game(void)
{
	t_game	*game;

	if ((game = ft_memalloc(sizeof(t_game))) == NULL)
		return (NULL);
	game->on = 1;
	game->p = 0;
	game->map = NULL;
	game->moves = NULL;
	game->token = NULL;
	game->mlx = NULL;
	ft_bzero((void*)&game->token_dim, sizeof(size_t) * 2);
	ft_bzero((void*)&game->offset, sizeof(size_t) * 4);
	ft_bzero((void*)&game->dim, sizeof(size_t) * 2);
	determine_player(game);
	return (game);
}

void		determine_player(t_game *game)
{
	char	*line;
	char	*ptr;

	line = NULL;
	if (game->on)
	{
		if (get_next_line(0, &line) < 1)
			game->on = 0;
		if (game->on)
		{
			ptr = line;
			while (!ft_isdigit(*ptr))
				ptr++;
			game->p = ft_atoi(ptr);
		}
		ft_strdel(&line);
		if (game->p == 1)
			game->piece = 'o';
		else if (game->p == 2)
			game->piece = 'x';
		if (game->piece == 'x')
			game->opp = 'o';
		else
			game->opp = 'x';
	}
}
