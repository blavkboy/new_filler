/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calculate.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/04 17:17:12 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/07/09 14:42:44 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/player.h"

long long		distance(t_moves *move, size_t y, size_t x)
{
	return (ft_power(move->play[1] - x, 2) + ft_power(move->play[0] - y, 2));
}

t_moves			*dst_compare(long long *len, t_game *game, t_moves *moves)
{
	size_t		y;
	size_t		x;
	long long	start;

	y = 0;
	start = *len;
	while (y < game->dim[0])
	{
		x = 0;
		while (x < game->dim[1])
		{
			if (ft_tolower(game->map[y][x]) == game->opp)
				if (distance(moves, y, x) < start)
					start = distance(moves, y, x);
			x++;
		}
		y++;
	}
	if (*len > start)
	{
		*len = start;
		return (moves);
	}
	return (NULL);
}
